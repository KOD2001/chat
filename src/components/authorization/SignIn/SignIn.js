import React from 'react';
import {useFormik} from 'formik';
import * as yup from 'yup';
import { useDispatch, useSelector} from 'react-redux';
import '../authorization.css';
import './icons.css';
import { Button, Label, Input } from 'reactstrap';
import { signInAction, googleSignInAction } from '../../../redux/actions/authActions';
import { Link } from 'react-router-dom';

export default function SignIn() {

  const dispatch = useDispatch();
  const signInError = useSelector(state => state.authReducer.signIn_error_message);

  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    validationSchema: yup.object({
      email: yup.string()
        .email('Invalid email format')
        .required('Required!'),
      password: yup.string()
        .required('Required!')
        .matches(/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
         'A password must contains at least eight characters, including at least one number and includes both lower and uppercase letters')
    }),
    onSubmit: (value) => {dispatch(signInAction({
      email: value.email,
      password: value.password
    }))}
  });

  return (
    <div className='container'>
      <form className='form' onSubmit={formik.handleSubmit}>
      {JSON.stringify(signInError) !== '{}' ? <p style={{marginTop: '10px'}}>{signInError}</p> : null}
        <h1>Sign in</h1>
        <div>
          <Label>Email</Label>
          <Input
            type='email'
            name='email'
            value={formik.values.email}
            onChange={formik.handleChange}
          />
          {formik.errors.email && formik.touched.email && (
            <p>{formik.errors.email}</p>
          )}
        </div>
        <div>
          <Label>Password</Label>
          <Input
            type='password'
            name='password'
            value={formik.values.password}
            onChange={formik.handleChange}
          />
          {formik.errors.password && formik.touched.password && (
            <p>{formik.errors.password}</p>
          )}
        </div>
        <div>
          <Button type='submit' color='primary' size='lg'>Sign in</Button>
        </div>
        <div className='form__social-networks-auth'>
          <div onClick={() => {dispatch(googleSignInAction())}} className='icon-google'></div>
        </div>
        <div className='form__additional-links'>
          <Link to='/signUp'>Sign Up</Link>
          <Link to='/forgotPassword'>Forgot password?</Link>
        </div>
      </form>
    </div>
  );
}