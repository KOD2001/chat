import React from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import '../authorization.css';
import { Button, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import { signUpAction } from '../../../redux/actions/authActions';

export default function SignUp() {
  const dispatch = useDispatch();
  const signUpError = useSelector(
    (state) => state.authReducer.signUp_error_message,
  );

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      confirmPassword: '',
    },
    validationSchema: yup.object({
      email: yup.string().email('Invalid email format').required('Required!'),
      password: yup
        .string()
        .required('Required!')
        .matches(
          /^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
          'A password must contains at least eight characters, including at least one number and includes both lower and uppercase letters',
        ),
      confirmPassword: yup
        .string()
        .required('Required!')
        .test('passwords-match', 'Passwords must match', function (value) {
          return this.parent.password === value;
        }),
    }),
    onSubmit: (value) => {
      dispatch(
        signUpAction({
          email: value.email,
          password: value.password,
        }),
      );
    },
  });

  return (
    <div className="container">
      <form className="form" onSubmit={formik.handleSubmit}>
        {JSON.stringify(signUpError) !== '{}' ? (
          <p style={{ marginTop: '10px' }}>{signUpError}</p>
        ) : null}
        <h1>Sign up</h1>
        <div>
          <Label>Email</Label>
          <Input
            type="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
          />
          {formik.errors.email && formik.touched.email && (
            <p>{formik.errors.email}</p>
          )}
        </div>
        <div>
          <Label>Password</Label>
          <Input
            type="password"
            name="password"
            value={formik.values.password}
            onChange={formik.handleChange}
          />
          {formik.errors.password && formik.touched.password && (
            <p>{formik.errors.password}</p>
          )}
        </div>
        <div>
          <Label>Confirm password</Label>
          <Input
            type="password"
            name="confirmPassword"
            value={formik.values.confirmPassword}
            onChange={formik.handleChange}
          />
          {formik.errors.confirmPassword && formik.touched.confirmPassword && (
            <p>{formik.errors.confirmPassword}</p>
          )}
        </div>
        <div>
          <Button type="submit" color="primary" size="lg">
            Sign up
          </Button>
        </div>
        <div className="form__additional-links">
          <Link to="/signIn">Sign In</Link>
          <Link to="/forgotPassword">Forgot password?</Link>
        </div>
      </form>
    </div>
  );
}
