import React from 'react';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import '../authorization.css';
import { Button, Label, Input } from 'reactstrap';
import { Link } from 'react-router-dom';
import { forgotPasswordAction } from '../../../redux/actions/authActions';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function ForgotPassword() {
  const dispatch = useDispatch();

  toast.configure();

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: yup.object({
      email: yup.string().email('Invalid email format').required('Required!'),
    }),
    onSubmit: (value) => {
      dispatch(
        forgotPasswordAction({
          email: value.email,
        }),
      );
      toast.success('Please check your email for a password recovery message', {
        position: toast.POSITION.TOP_CENTER,
        autoClose: false,
      });
    },
  });

  return (
    <div className="container">
      <form className="form" onSubmit={formik.handleSubmit}>
        <h2>Forgot password</h2>
        <div>
          <Label>Email</Label>
          <Input
            type="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
          />
          {formik.errors.email && formik.touched.email && (
            <p>{formik.errors.email}</p>
          )}
        </div>
        <div>
          <Button type="submit" color="primary" size="lg">
            Submit
          </Button>
        </div>
        <div className="form__additional-links">
          <Link to="/signIn">Sign in</Link>
          <Link to="/signUp">Sign up</Link>
        </div>
      </form>
    </div>
  );
}
