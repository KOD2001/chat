import React from 'react';
import {useFormik} from 'formik';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import '../authorization.css';
import { Button, Label, Input } from 'reactstrap';
import { Link, Switch, Redirect } from 'react-router-dom';
import { resetPasswordAction } from '../../../redux/actions/authActions';

export default function ResetPassword() {

  const dispatch = useDispatch();
  
  const passwordIsChanged = useSelector(state => state.authReducer.passwordIsChanged);

  const formik = useFormik({
    initialValues: {
      password: '',
      confirmPassword: ''
    },
    validationSchema: yup.object({
      password: yup.string()
        .required('Required!')
        .matches(/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
         'A password must contains at least eight characters, including at least one number and includes both lower and uppercase letters'),
      confirmPassword: yup.string()
        .required('Required!')
        .test('passwords-match', 'Passwords must match', function(value){
          return this.parent.password === value;
        })
    }),
    onSubmit: function(value) {dispatch(resetPasswordAction({
        password: value.password,
        actionCode: new URLSearchParams(window.location.search).get('oobCode')
      }))}
  });

  return !passwordIsChanged ? (
    <div className='container'>
      <form className='form' onSubmit={formik.handleSubmit}>
        <h2>Reset password</h2>
        <div>
          <Label>Password</Label>
          <Input
            type='password'
            name='password'
            value={formik.values.password}
            onChange={formik.handleChange}
          />
          {formik.errors.password && formik.touched.password && (
            <p>{formik.errors.password}</p>
          )}
        </div>
        <div>
          <Label>Confirm password</Label>
          <Input
            type='password'
            name='confirmPassword'
            value={formik.values.confirmPassword}
            onChange={formik.handleChange}
          />
          {formik.errors.confirmPassword && formik.touched.confirmPassword && (
            <p>{formik.errors.confirmPassword}</p>
          )}          
        </div>
        <div>
          <Button type='submit' color='primary' size='lg'>Reset</Button>
        </div>
        <div className='form__additional-links'>
          <Link to='/signIn'>Sign In</Link>
          <Link to='/signUp'>Sign Up</Link>
        </div>
      </form>
    </div>
  )
  :
  (
    <Switch>
            <Redirect to='/signIn'/>
    </Switch>
  )
}