import React from 'react';
import Layout from './layout/Layout';
import 'firebase/database';
import { useSelector } from 'react-redux';
import Chat from './chat/Chat';

const Main = () => {
  const currentChatRoomKey = useSelector(
    (state) => state.chatReducer.currentChatRoomKey,
  );

  return currentChatRoomKey === null ? (
    <>
      <Layout />
    </>
  ) : (
    <>
      <Layout />
      <Chat chatRoomKey={currentChatRoomKey} />
    </>
  );
};

export default Main;
