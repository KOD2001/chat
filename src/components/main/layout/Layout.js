import React, { useState } from 'react';
import './layout.css';
import RoomList from '../roomList/roomList';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'reactstrap';
import { signOutAction } from '../../../redux/actions/authActions';
import CreateRoomModal from '../Modals/createRoomModal';

export default function Layout() {
  const dispatch = useDispatch();

  const user = useSelector((state) => state.authReducer.user);

  const [modal, setModal] = useState(false);
  return (
    <>
      <CreateRoomModal modal={modal} setModal={setModal} />
      <div className="layout">
        <div className="layout__up-navbar">
          <Button
            className="layout__create-room"
            onClick={() => setModal(!modal)}
            type="submit"
            outline
            color="info"
            size="lg">
            Create room
          </Button>
          <h2 className="layout__display-name">{user.email}</h2>
          <Button
            className="layout__sign-out"
            onClick={() => dispatch(signOutAction())}
            type="submit"
            color="primary"
            size="lg">
            Sign out
          </Button>
        </div>
      </div>
      <RoomList />
    </>
  );
}
