import React, { useEffect, useState } from 'react';
import './roomList.css';
import { ListGroup, Input } from 'reactstrap';
import RoomListItem from './roomListItem';
import firebase from 'firebase/app';
import 'firebase/database';
import { useDispatch, useSelector } from 'react-redux';
import { loadRoomsDataAction } from '../../../redux/actions/chatActions';

const RoomList = () => {
  const dispatch = useDispatch();
  const rooms = useSelector((state) => state.chatReducer.rooms);
  const [value, setValue] = useState('');
  const [roomsNotFound, setRoomsNotFound] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const database = firebase.database();
      const roomsRef = database.ref('/');
      roomsRef.on('value', (snapshot) => {
        if (snapshot.exists()) {
          dispatch(loadRoomsDataAction(Object.entries(snapshot.val())));
        } else {
          dispatch(loadRoomsDataAction(null));
          setRoomsNotFound('Rooms not found');
        }
      });
    };
    fetchData();
  }, []);

  const debounce = (fn, ms) => {
    let timeout;
    return function () {
      const fnCall = () => {
        fn.apply(this, arguments);
      };
      clearTimeout(timeout);
      timeout = setTimeout(fnCall, ms);
    };
  };

  const onChange = (e) => {
    setValue(e.target.value);
  };

  const onChangeDebounced = debounce(onChange, 500);

  return rooms === null ? (
    roomsNotFound === '' ? (
      <div className="lds-dual-ring"></div>
    ) : (
      <div className="roomsNotFound">{roomsNotFound}</div>
    )
  ) : (
    <>
      <div className="roomList__additionals">
        <h2>Rooms:</h2>
        <Input
          id="roomList__input"
          type="text"
          name="findRoom"
          placeholder="find room"
          onChange={onChangeDebounced}></Input>
      </div>
      <ListGroup id="d-inline-flex" className="roomList">
        {rooms
          .filter((item) => {
            if (item === '') {
              return item;
            } else if (
              item[1].name.toLowerCase().includes(value.toLowerCase().trim())
            ) {
              return item;
            }
          })
          .map((item, key) => {
            const chatRoomKey = item[0];
            const chatRoomName = item[1].name;
            const chatRoomPassword = item[1].password;
            return (
              <RoomListItem
                key={key}
                chatRoomKey={chatRoomKey}
                chatRoomName={chatRoomName}
                chatRoomPassword={chatRoomPassword}></RoomListItem>
            );
          })}
      </ListGroup>
    </>
  );
};

export default RoomList;
