import SignIn from './components/authorization/SignIn/SignIn';
import SignUp from './components/authorization/SignUp/SignUp';
import Main from './components/main/Main';
import ForgotPassword from './components/authorization/forgotPassword/ForgotPassword';
import ResetPassword from './components/authorization/resetPassword/ResetPassword';

export const publicRoutes = [
  {
    path: '/signIn',
    Component: SignIn,
  },

  {
    path: '/signUp',
    Component: SignUp,
  },

  {
    path: '/forgotPassword',
    Component: ForgotPassword,
  },

  {
    path: '/resetPassword',
    Component: ResetPassword,
  },
];

export const privateRoutes = [
  {
    path: '/main',
    Component: Main,
  },
];
